use std::path::{Path, PathBuf};
use std::{fs, io};

use anyhow::Context;
use configparser::ini::Ini;
use duct::{cmd, Expression};
use nix::libc;
use xdg::BaseDirectories;

macro_rules! concat_os_strings {
    ($($os_str:expr),+ $(,)?) => ({
        let mut result = std::ffi::OsString::with_capacity($($os_str.len() +)+ 0);
        $(result.push($os_str);)+

        result
    });
}

/// Concatenates one or several [`std::ffi::OsString`].
pub(crate) use concat_os_strings;

/// Retrieves the configured terminal emulator as an [`Expression`].
pub fn get_terminal_emulator_expression() -> Expression {
    // Only supports XFCE, but the point of "os-customizations" is to be specific about my needs :)

    /// The default terminal used in XFCE.
    const DEFAULT_TERMINAL: &str = "xfce4-terminal";

    /// Retrieves the default emulator in case none has been configured.
    fn get_default_emulator() -> Expression {
        cmd!(DEFAULT_TERMINAL, "--disable-server")
    }

    let Ok(directory) = BaseDirectories::with_prefix("xfce4") else {
        return get_default_emulator();
    };

    let Ok(helpers) = fs::read_to_string(directory.get_config_file("helpers.rc")) else {
        return get_default_emulator();
    };

    let Ok(helpers) = Ini::new().read(helpers) else {
        return get_default_emulator();
    };

    helpers
        .get("default")
        .and_then(|e| e.get("terminalemulator"))
        .and_then(|v| v.as_ref())
        .map(|v| v.trim())
        .filter(|v| *v != DEFAULT_TERMINAL)
        .map(|v| cmd!(v))
        .unwrap_or_else(get_default_emulator)
}

/// Evaluates the supplied derivation against the given flake and returns its output path.
pub fn eval_derivation(flake: &str, package: &str) -> anyhow::Result<PathBuf> {
    Ok(cmd!(
        "nix",
        "eval",
        concat_os_strings!(flake, "#", package),
        "--apply",
        "toString",
        "--raw",
        "--no-write-lock-file",
        "--show-trace"
    )
    .read()
    .context("Could not run 'nix eval'")?
    .into())
}

/// Copies the supplied file or directory to the given destination, ignoring symbolic links and socket files.
/// The destination should not exist and the source file will be moved into it.
pub fn copy_recursive(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    let src = src.as_ref();
    let dst = dst.as_ref();

    match fs::read_dir(src) {
        Ok(directory) => {
            fs::create_dir(dst)?;

            for entry in directory {
                let path = entry?.path();

                copy_recursive(&path, dst.join(path.file_name().unwrap()))?;
            }

            Ok(())
        }

        Err(e) if e.raw_os_error() == Some(libc::ENOTDIR) => {
            if let Err(e) = fs::copy(src, dst) {
                if e.raw_os_error() != Some(libc::ENXIO) {
                    return Err(e);
                }
            }

            Ok(())
        }

        Err(e) => Err(e),
    }
}
