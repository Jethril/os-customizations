use std::collections::HashMap;
use std::ffi::{OsStr, OsString};
use std::fs::File;
use std::path::{Path, PathBuf};
use std::{env, fs};

use anyhow::{anyhow, bail, Context};
use duct::cmd;
use nix::libc::pid_t;
use nix::sys::signal::{kill, Signal};
use nix::unistd::Pid;
use tempfile::TempDir;
use xdg::BaseDirectories;

use crate::cli::Tool;
use crate::globals::PACKAGES_REGISTRY_NAME;
use crate::misc;
use crate::misc::concat_os_strings;

/// Starts the supplied tool and informs the parent project-runner instance that the operation is successful.
pub fn start(tool: Tool, runner_pid: pid_t) -> anyhow::Result<()> {
    // === Project name ===
    let current_dir = env::current_dir().context("Could not retrieve the current directory")?;
    let project_name = current_dir
        .file_name()
        .map(|f| f.to_os_string())
        .unwrap_or_else(|| OsString::from("fs-root"));

    // === docker-compose ===
    run_docker_compose_if_needed(&current_dir).context("Unable to run 'docker-compose'")?;

    // === GUI handling ===
    let _ = kill(Pid::from_raw(runner_pid), Signal::SIGUSR2);

    // === Tool startup ===
    match tool {
        Tool::Shell => start_shell()?,
        Tool::Zed => start_zed()?,
        Tool::Clion => start_clion(&project_name)?,
        Tool::Rider => start_rider(&project_name)?,
        Tool::Idea => start_idea(&project_name)?,
    }

    Ok(())
}

/// Runs `docker-compose` on the current directory, if a `docker-compose.yml` file is found.
fn run_docker_compose_if_needed(current_dir: &Path) -> anyhow::Result<()> {
    if !current_dir.join("docker-compose.yml").is_file() && !current_dir.join("../../docker-compose.yml").is_file() {
        println!("No docker-compose file was found, skipping.");

        return Ok(());
    }

    // TODO Down docker-compose when the tool exits
    println!("A docker-compose file was found, running 'docker-compose up'.");
    cmd!("pkexec", "--keep-cwd", "docker-compose", "up", "--detach")
        .run()
        .context("Could not run the 'docker-compose' executable")?;

    Ok(())
}

/// Runs the configured terminal emulator.
fn start_shell() -> anyhow::Result<()> {
    misc::get_terminal_emulator_expression()
        .run()
        .context("Could not run the shell")?;

    Ok(())
}

/// Runs Zed.
fn start_zed() -> anyhow::Result<()> {
    println!("Running Zed.");
    cmd!("zed", ".", "--wait")
        .run()
        .context("Could not run the 'zed' executable")?;

    Ok(())
}

/// Runs JetBrains' CLion.
fn start_clion(project_name: &OsStr) -> anyhow::Result<()> {
    let (paths, tool_env_var, props_file_path) = prepare_intellij_based_tool(project_name, "clion")?;

    println!("Patching 'intellij-rust-native-helper'...");
    let patch_elf_res = patch_elf_in_place(
        &paths
            .plugins_path
            .join("intellij-rust/bin/linux/x86-64/intellij-rust-native-helper"),
    );

    if let Err(err) = patch_elf_res {
        println!("Warning: could not patch 'intellij-rust-native-helper' due to a failure: {err}");
    }

    println!("Running CLion.");
    cmd!("clion", ".")
        .env(tool_env_var, props_file_path)
        .run()
        .context("Unable to run the 'clion' executable")?;

    Ok(())
}

/// Runs JetBrains' Rider.
fn start_rider(project_name: &OsStr) -> anyhow::Result<()> {
    let solution_file = fs::read_dir(".")
        .context("Unable to list the current directory")?
        .filter_map(|res| res.ok())
        .filter(|e| e.file_type().map(|t| t.is_file()).unwrap_or(false))
        .map(|e| e.path())
        .find(|p| p.extension() == Some(OsStr::new("sln")));

    let Some(solution_file_name) = solution_file.as_ref().and_then(|f| f.file_name()) else {
        bail!("The project directory contains no '.sln' file!");
    };

    let (_paths, tool_env_var, props_file_path) = prepare_intellij_based_tool(project_name, "rider")?;

    println!("Running Rider.");
    cmd!("rider", solution_file_name)
        .env(tool_env_var, props_file_path)
        .run()
        .context("Unable to run the 'rider' executable")?;

    Ok(())
}

/// Runs IntelliJ IDEA.
fn start_idea(project_name: &OsStr) -> anyhow::Result<()> {
    let (_paths, tool_env_var, props_file_path) = prepare_intellij_based_tool(project_name, "idea")?;

    println!("Running IntelliJ IDEA.");

    cmd!("idea", ".")
        .env(tool_env_var, props_file_path)
        .run()
        .context("Unable to run the 'idea' executable")?;

    Ok(())
}

/// Prepares a temporary IDE directory and a command used to call the JetBrains tool with the supplied project name,
/// tool name and executable.
///
/// Although this function provides a workaround for https://youtrack.jetbrains.com/issue/IDEA-35480, it allows running
/// two identical JetBrains tools (e.g two Clion instances) within two different Nix shells.
fn prepare_intellij_based_tool(
    proj_name: &OsStr,
    tool_name: &str,
) -> anyhow::Result<(IntellijBasedToolPaths, String, PathBuf)> {
    macro_rules! req_prop {
        ($properties:expr, $name:literal) => {
            $properties
                .get($name)
                .ok_or_else(|| anyhow!(concat!("Property '", $name, "' is required")))
                .map(|v| substitute_properties_variables(v))
        };
    }

    macro_rules! set_prop {
        ($properties:expr, $name:literal, $path:expr) => {
            $properties.insert($name.to_owned(), $path.to_str().unwrap().to_owned())
        };
    }

    // ======== PROPERTIES FILE READING ========
    let mut tool_env_var = tool_name.to_ascii_uppercase();
    tool_env_var.push_str("_PROPERTIES");

    let Some(properties_path) = env::var_os(&tool_env_var) else {
        bail!("The environment variable '{tool_env_var}' is not set");
    };

    // TODO Project windows/state not remembered?

    let props_file =
        File::open(&properties_path).with_context(|| format!("Unable to open file '{properties_path:?}'"))?;

    let mut properties: HashMap<String, String> =
        serde_java_properties::from_reader(props_file).context("Unreadable properties file")?;

    let orig_config_path = PathBuf::from(req_prop!(properties, "idea.config.path")?);

    // Require those properties only, no need to retrieve them.
    let plugins_path = PathBuf::from(req_prop!(properties, "idea.plugins.path")?);
    req_prop!(properties, "idea.log.path")?;

    // ======== IDE DIRS HANDLING ========
    let temp_ide_dir = tempfile::tempdir().context("Could not create the IDE settings temporary directory")?;
    let config_path = temp_ide_dir.path().join(orig_config_path.file_name().unwrap());
    let system_path = BaseDirectories::new()
        .context("Could not retrieve the XDG base directories")?
        .create_cache_directory(concat_os_strings!(tool_name, "-", proj_name, "-sysdir"))
        .context("Could not create the project cache directory")?;

    misc::copy_recursive(&orig_config_path, &config_path)
        .context("Could not copy to the IDE settings temporary directory")?;

    // Delete the PID files, if they exist.
    let _ = fs::remove_file(config_path.join(".lock"));

    // ======== PROPERTIES FILE WRITING ========
    let dirs = IntellijBasedToolPaths {
        config_path,
        system_path,
        plugins_path,
        temp_ide_dir,
    };

    let props_file_path = dirs.temp_ide_dir.path().join("idea.properties");
    let props_file = File::create(&props_file_path).context("Unable to create the temporary properties file")?;

    set_prop!(properties, "idea.config.path", dirs.config_path);
    set_prop!(properties, "idea.system.path", dirs.system_path);

    serde_java_properties::to_writer(&properties, props_file).context("Unable to serialize the properties")?;

    Ok((dirs, tool_env_var, props_file_path))
}

/// Substitutes the supported variables (Java properties placeholder format).
fn substitute_properties_variables(value: &str) -> String {
    if let Ok(home) = env::var("HOME") {
        value.replace("${user.home}", &home)
    } else {
        value.to_owned()
    }
}

/// Updates the ELF at the supplied path and replaces its interpreter with the glibc ld.
fn patch_elf_in_place(path: &Path) -> anyhow::Result<()> {
    let mut interpreter_path = misc::eval_derivation(PACKAGES_REGISTRY_NAME, "glibc")?;
    interpreter_path.push("lib64/ld-linux-x86-64.so.2");

    cmd!("patchelf", "--set-interpreter", interpreter_path, path)
        .run()
        .context("Could not run the 'patchelf' executable")?;

    Ok(())
}

/// Structure that holds the different paths the IDE should use.
struct IntellijBasedToolPaths {
    /// The temporary directory in which most IDE files will be stored.
    temp_ide_dir: TempDir,

    /// The IDE's configuration path, inside `tmp_ide_dir`.
    config_path: PathBuf,

    /// The IDE's system path, usually in the `~/.cache` directory.
    system_path: PathBuf,

    /// The IDE's plugins path, inside `tmp_ide_dir`.
    plugins_path: PathBuf,
}
