use std::path::PathBuf;
use std::{env, fs, process};

use anyhow::{bail, Context};
use clap::Parser;
use nix::libc;
use nix::libc::pid_t;
use nix::sys::signal;
use nix::sys::signal::{SigHandler, Signal};

use cli::{Command, Tool};
use flake::NixDevelop;
use globals::{ENFORCED_TOOL_ENV, RUNNER_PID_ENV};
use gui::Gui;

mod cli;
mod flake;
mod globals;
mod gui;
mod misc;
mod tool;

fn main() {
    let result = match Command::parse() {
        Command::Open {
            flake,
            enforce_tool,
            no_gui,
        } => open(flake, enforce_tool, no_gui),

        Command::StartTool { tool } => start_tool(tool),
    };

    if let Err(err) = result {
        eprintln!("Error: {err:?}");
        process::exit(3);
    }
}

/// Code for the `project-runner open` command.
fn open(mut flake_path: PathBuf, enforce_tool: Option<Tool>, no_gui: bool) -> anyhow::Result<()> {
    if !flake_path.is_file() {
        bail!("The supplied flake path is not a valid file.");
    }

    let _ = unsafe { signal::signal(Signal::SIGUSR2, SigHandler::Handler(on_sigusr2)) };

    if !flake_path.is_absolute() {
        flake_path = fs::canonicalize(flake_path)?;
    }

    let gui = if no_gui { None } else { Some(Gui::show()?) };
    let nix_develop_res = NixDevelop::new(flake_path, enforce_tool);

    let Some(gui) = gui else {
        return nix_develop_res.and_then(NixDevelop::run).context("Unable to run 'nix develop'");
    };

    gui.run(nix_develop_res).context("Unable to run the GUI")?;
    Ok(())
}

/// Code for the `project-runner start-tool` command.
fn start_tool(mut tool: Tool) -> anyhow::Result<()> {
    if let Some(t) = env::var(ENFORCED_TOOL_ENV).ok().and_then(|s| s.parse().ok()) {
        tool = t;
    }

    let Some(runner_pid) = env::var(RUNNER_PID_ENV).ok().and_then(|s| s.parse::<pid_t>().ok()) else {
        bail!("Invalid or absent '{RUNNER_PID_ENV}' environment variable.");
    };

    tool::start(tool, runner_pid).context("Unable to start the supplied tool")?;

    Ok(())
}

/// SIGUSR2 handler which hides the GUI.
/// Called from the the project-runner itself, when run with the start-tool subcommand.
extern "C" fn on_sigusr2(_: libc::c_int) {
    gui::signal_sigusr2();
}
