use arboard::Clipboard;
use static_init::dynamic;

/// The name of the environment variable used to tell the tool opener which tool should be started instead.
pub const ENFORCED_TOOL_ENV: &str = "PROJECT_RUNNER_ENFORCE_TOOL";

/// The name of the environment variable used to tell the tool opener the PID of this process.
pub const RUNNER_PID_ENV: &str = "PROJECT_RUNNER_PID";

/// The flake used to get packages used by this program.
pub const PACKAGES_REGISTRY_NAME: &str = "jethpkgs";

/// Lazily initialized clipboard manager.
#[dynamic(lazy, drop)]
pub static mut CLIPBOARD: Option<Clipboard> = Clipboard::new().ok();
