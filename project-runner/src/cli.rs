use std::path::PathBuf;

/// The root CLI command.
#[derive(Debug, Clone, clap::Parser)]
#[command(author, version, about)]
pub enum Command {
    /// Opens the supplied Nix flake file and starts a GUI that displays the progress and/or errors.
    Open {
        /// The path of the flake file.
        flake: PathBuf,

        /// Enforces the supplied tool even if the flake is configured to start something else.
        #[arg(long)]
        enforce_tool: Option<Tool>,

        /// Hides the GUI and shows a command-line output.
        #[arg(long)]
        no_gui: bool,
    },

    /// Starts the supplied tool. Must be called from within a flake initialized with the "open" command.
    StartTool {
        /// The tool to start.
        tool: Tool,
    },
}

/// The different tools that are supported by this program.
#[derive(Debug, strum::Display, Clone, clap::ValueEnum, strum::EnumString)]
pub enum Tool {
    /// A simple command-line shell.
    #[strum(serialize = "shell")]
    Shell,

    /// Zed text editor / lightweight IDE.
    #[strum(serialize = "zed")]
    Zed,

    /// JetBrains' CLion IDE, for C, C++ and Rust.
    #[strum(serialize = "clion")]
    Clion,

    /// JetBrains' Rider IDE, for the .NET ecosystem.
    #[strum(serialize = "rider")]
    Rider,

    /// JetBrains' IntelliJ IDEA IDE, for the JVM ecosystem.
    #[strum(serialize = "idea")]
    Idea,
}
