use anyhow::{anyhow, bail, Context};
use duct::cmd;
use nix::libc::pid_t;
use nix::sys::select::{select, FdSet};
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;
use std::ffi::{OsStr, OsString};
use std::fmt::{Display, Write};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::mem::ManuallyDrop;
use std::os::fd::{AsRawFd, FromRawFd, RawFd};
use std::os::unix::prelude::ExitStatusExt;
use std::path::Path;
use std::process::{Child, Command, Stdio};
use std::thread::JoinHandle;
use std::{fs, process, thread};
use tempfile::TempDir;

use super::cli::Tool;
use super::globals::{ENFORCED_TOOL_ENV, RUNNER_PID_ENV};
use super::misc::concat_os_strings;

/// URI scheme used for flakes that are based on paths.
const PATH_URI_SCHEME: &str = "path:";

/// Attribute used to determine whether the flake uses project-runner.
const FLAKE_USES_PROJECT_RUNNER_ATTR: &str = "#usesProjectRunner";

/// Structure that represents the "nix develop" program.
pub struct NixDevelop {
    /// The process' working directory, when different from the flake's directory.
    work_dir: Option<TempDir>,

    /// The program's command-line arguments.
    command: Command,
}

impl NixDevelop {
    /// Creates a new [`NixDevelop`] program.
    ///
    /// # Parameters
    ///  - `flake_path`: the path of the flake that will be run;
    ///  - `enforce_tool`: an optional tool to use, if the flake uses project-runner.
    pub fn new<P: AsRef<Path>>(flake_path: P, enforce_tool: Option<Tool>) -> anyhow::Result<Self> {
        let flake_path = flake_path.as_ref();
        let project_dir = flake_path
            .parent()
            .ok_or_else(|| anyhow!("The supplied flake path does not have any parent"))?;

        let work_dir = create_work_dir_if_needed(flake_path)?;
        let command = build_command(project_dir, work_dir.as_ref(), enforce_tool);

        Ok(Self {
            work_dir: work_dir.map(|v| v.0),
            command,
        })
    }

    /// Runs the program and waits for it to exit.
    /// The started process inherits its standard input and outputs from the parent process.
    ///
    /// # Errors
    /// Returns an error if the process could not be started or it exited with an unsuccessful code or signal.
    pub fn run(mut self) -> anyhow::Result<()> {
        let status = self.command.status()?;

        if status.success() {
            return Ok(());
        }

        if let Some(code) = status.code() {
            bail!("'nix develop' exited with code {code}");
        }

        let signal = status.signal().expect("it should be a signal");
        let core_dump = if status.core_dumped() { " (core dumped)" } else { "" };
        let signal = Signal::try_from(signal)
            .map(|s| s.to_string())
            .unwrap_or_else(|_| signal.to_string());

        bail!("'nix develop' exited with signal {signal}{core_dump}");
    }

    /// Starts the program without waiting it.
    /// Any failure during the program's startup is reported using `line_cb`.
    ///
    /// # Parameters
    ///  - `line_cb`: a callback function that will be called each time the process writes a line to one of its outputs;
    ///  - `exit_cb`: a callback function that will be called when the process exits. A [`None`] indicates that it
    ///    exited successfully, whereas a [`Some`] contains a failure message to be displayed.
    pub fn start(
        mut self,
        line_cb: impl Fn(String) + Send + 'static,
        exit_cb: impl FnOnce(Option<String>) + Send + 'static,
    ) -> NixDevelopProcess {
        let child = self
            .command
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .stdin(Stdio::null())
            .spawn();

        let child = match child {
            Ok(c) => c,
            Err(err) => {
                read_error("Unable to read from", err, line_cb);

                return NixDevelopProcess::DidNotStart;
            }
        };

        let pid = child.id() as pid_t;
        let stdout = child.stdout.as_ref().unwrap().as_raw_fd();
        let stderr = child.stderr.as_ref().unwrap().as_raw_fd();

        let read_thread_handle = thread::spawn(move || {
            let res = read_inputs_callback(
                pid,
                [stdout, stderr],
                |line| {
                    println!("{line}");
                    line_cb(line);
                },
                exit_cb,
            );

            if let Err(err) = res {
                read_error("Unable to read the inputs:", err, line_cb);
            }
        });

        NixDevelopProcess::Started {
            work_dir: self.work_dir,
            child,
            read_thread_handle,
        }
    }
}

/// The Nix develop process.
pub enum NixDevelopProcess {
    /// The process did not start due to a command failure.
    DidNotStart,

    /// The process was started.
    Started {
        work_dir: Option<TempDir>,
        child: Child,
        read_thread_handle: JoinHandle<()>,
    },
}

/// Creates a temporary working directory, if the flakes uses the project runner.
/// Returns an optional tuple containing the working directory with the flake's URI. The returned option is [`None`]
/// when the flake doesn't use the project runner.
///
/// In practice, this is a workaround for https://github.com/NixOS/nix/issues/3121.
fn create_work_dir_if_needed(flake_path: &Path) -> anyhow::Result<Option<(TempDir, OsString)>> {
    let lock_path = flake_path.with_extension("lock");
    let work_dir = tempfile::tempdir().context("Could not create the temp dir")?;
    let work_dir_path = work_dir.path();
    let work_dir_flake_uri = concat_os_strings!(PATH_URI_SCHEME, work_dir_path.as_os_str());

    fs::copy(flake_path, work_dir_path.join("flake.nix")).context("Could not create the flake file")?;
    let _ = fs::copy(lock_path, work_dir_path.join("flake.lock"));

    Ok(uses_project_runner(&work_dir_flake_uri)
        .context("Could not retrieve the flake's attributes")?
        .then_some((work_dir, work_dir_flake_uri)))
}

/// Determines whether the flake pointed by the supplied flake URI uses the project runner.
fn uses_project_runner(flake_uri: &OsStr) -> anyhow::Result<bool> {
    let output_str = cmd!(
        "nix",
        "eval",
        concat_os_strings!(flake_uri, FLAKE_USES_PROJECT_RUNNER_ATTR),
        "--show-trace"
    )
    .read()
    .context("Could not run the 'nix' executable")?;

    Ok(output_str.trim() == "true")
}

/// Builds and returns a new [`Command`] that runs `nix develop`.
///
/// # Parameters
///  - `project_dir`: the project directory in which the tools should start;
///  - `workdir`: an optional working directory that contains the flake (else, `project_dir` is used instead);
///  - `enforce_tool`: a tool to enforce, even if the flake is configured to run something else. Only works in flakes
///    that use the project runner. Silently fails if that's not the case.
fn build_command(project_dir: &Path, work_dir: Option<&(TempDir, OsString)>, enforce_tool: Option<Tool>) -> Command {
    let mut cmd = Command::new("nix");

    cmd.arg("develop")
        .arg("--impure")
        .arg("--print-build-logs")
        .arg("--show-trace");

    if let Some((_, uri)) = work_dir {
        cmd.arg(uri).env(RUNNER_PID_ENV, process::id().to_string());

        match enforce_tool {
            Some(tool) => cmd.env(ENFORCED_TOOL_ENV, tool.to_string()),
            None => cmd.env_remove(ENFORCED_TOOL_ENV),
        };
    } else {
        cmd.arg(concat_os_strings!(PATH_URI_SCHEME, "."));
    }

    cmd.arg("--command")
        .arg("bash")
        .arg("-c")
        .arg("exit 0")
        .current_dir(project_dir);

    cmd
}

/// Reads the supplied inputs and calls the given function for each line.
fn read_inputs_callback<const N: usize>(
    pid: pid_t,
    inputs: [RawFd; N],
    line_cb: impl Fn(String),
    exit_cb: impl FnOnce(Option<String>),
) -> anyhow::Result<()> {
    let mut buff = String::new();
    let mut inputs = inputs
        .into_iter()
        .map(|fd| ManuallyDrop::new(BufReader::new(unsafe { File::from_raw_fd(fd) })))
        .collect::<Vec<_>>();

    while !inputs.is_empty() {
        let mut rd_descriptors = FdSet::new();
        let mut i = 0;

        for input in &inputs {
            rd_descriptors.insert(input.get_ref().as_raw_fd());
        }

        let mut err_descriptors = rd_descriptors; // Implements `Copy`, so this is equivalent to a `clone()`.

        select(None, Some(&mut rd_descriptors), None, Some(&mut err_descriptors), None)?;

        while i < inputs.len() {
            let fd = inputs[i].get_ref().as_raw_fd();

            if !rd_descriptors.contains(fd) && !err_descriptors.contains(fd) {
                i += 1;
                continue;
            }

            match inputs[i].read_line(&mut buff) {
                Ok(read) if read > 0 => i += 1,

                Ok(_) => {
                    inputs.remove(i);
                }

                Err(e) => {
                    inputs.remove(i);
                    writeln!(&mut buff, "An error occurred while reading data: {e}")
                        .expect("should not happen on a string");
                }
            }

            line_cb(buff.trim_end().to_owned());
            buff.clear();
        }
    }

    match waitpid(Pid::from_raw(pid), None)? {
        WaitStatus::Exited(_, code) if code == 0 => exit_cb(None),
        WaitStatus::Exited(_, code) => exit_cb(Some(format!("PID {pid} exited with code {code}"))),
        WaitStatus::Signaled(_, signal, core_dump) => {
            let core_dump = if core_dump { " (core dumped)" } else { "" };

            exit_cb(Some(format!("PID {pid} exited with signal {signal}{core_dump}")));
        }
        _ => (),
    }

    Ok(())
}

/// Equivalent of [`read_inputs_callback`], but reads a [`Display`] objet instead of file descriptors.
fn read_error(msg: &str, error: impl Display, line_cb: impl Fn(String)) {
    let error = error.to_string();

    println!("{msg}: {error}");
    line_cb(msg.to_string());

    for line in error.lines() {
        line_cb(format!("  {line}"));
    }
}
