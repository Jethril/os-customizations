use crate::config::{EntryConfiguration, EntryContent, EntryKind};
use anyhow::{anyhow, bail, Context};
use nix::errno::Errno;
use nix::sys::stat;
use nix::sys::stat::{Mode, SFlag};
use nix::unistd::{Gid, Group, Uid, User};
use nix::{unistd, NixPath};
use std::ffi::OsStr;
use std::fs;
use std::fs::Permissions;
use std::os::unix;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};

/// Represents an in-memory entry, resolved from the configured one and used to compare to a concrete file system entry.
/// Used as a "mirror" to perform a side-by-side comparison between what has been configured and what's on the file
/// system.
struct VirtualFsEntry {
    /// The type of entry.
    kind: EntryKind,

    /// The entry's path.
    path: PathBuf,

    /// The entry's mode.
    mode: Mode,

    /// The entry's owner.
    owner: Uid,

    /// The entry's group.
    group: Gid,

    /// The entry's content.
    content: Option<Vec<u8>>,

    /// The permissions to use when creating the intermediate directories, or [`None`] when no intermediate directories
    /// should be created.
    directories_modes: Option<Mode>,
}

impl VirtualFsEntry {
    /// Creates a new [`VirtualFsEntry`] from the given configuration.
    fn create(config: &EntryConfiguration) -> anyhow::Result<Self> {
        let owner_name = config.owner.as_deref().unwrap_or("root");
        let group_name = config.group.as_deref().unwrap_or(owner_name);
        let owner = User::from_name(owner_name)
            .with_context(|| format!("Could not retrieve the UID of '{owner_name}'"))?
            .ok_or_else(|| anyhow!("User '{owner_name}' does not exist"))?;

        let path = if config.path.starts_with("~") {
            owner.dir.components().chain(config.path.components().skip(1)).collect()
        } else {
            config.path.clone()
        };

        if path.is_relative() {
            bail!("The path '{}' must be absolute", path.to_string_lossy());
        }

        Ok(Self {
            path,
            kind: config.kind,
            owner: owner.uid,

            content: if config.kind == EntryKind::Directory {
                None
            } else {
                Some(compute_entry_content(config).context("Could not read the entry content")?)
            },

            group: Group::from_name(group_name)
                .with_context(|| format!("Could not retrieve the GID of '{group_name}'"))?
                .ok_or_else(|| anyhow!("Group '{group_name}' does not exist"))?
                .gid,

            mode: config
                .mode
                .map(Mode::from_bits_truncate)
                .unwrap_or_else(|| get_default_permission(config.kind)),

            directories_modes: config.directories_modes.map(Mode::from_bits_truncate),
        })
    }
}

/// Represents a real file system entry.
enum FsEntry {
    /// The entry does not exists on the file system.
    NonExisting,

    /// The entry exists.
    Existing {
        /// The entry's type.
        kind: EntryKind,

        /// The entry's permissions.
        mode: Mode,

        /// The entry's owner.
        owner: Uid,

        /// The entry's group.
        group: Gid,

        /// The entry's content. Must be a path in case of a symlink, a string in case of a file and [`None`] in case of
        /// a directory.
        content: Option<Vec<u8>>,
    },
}

impl FsEntry {
    /// Creates this entry on the file system, if needed.
    fn create_if_needed(&mut self, model: &VirtualFsEntry) -> anyhow::Result<()> {
        if let FsEntry::Existing { .. } = self {
            return Ok(());
        }

        if let Some(dir_modes) = model.directories_modes {
            if let Some(parent) = model.path.parent() {
                create_dir_all(parent, dir_modes, model.owner, model.group)
                    .context("Could not create the directories")?;
            }
        }

        match model.kind {
            EntryKind::Symlink => unix::fs::symlink(
                PathBuf::from(OsStr::from_bytes(model.content.as_ref().unwrap())),
                &model.path,
            )?,

            EntryKind::File => fs::write(&model.path, model.content.as_ref().unwrap())?,
            EntryKind::Directory => fs::create_dir(&model.path)?,
        }

        *self = FsEntry::create(&model.path)?;
        self.update_if_needed(model)?;

        Ok(())
    }

    /// Updates this entry on the file system, if needed.
    fn update_if_needed(&mut self, model: &VirtualFsEntry) -> anyhow::Result<()> {
        let FsEntry::Existing { kind, mode, owner, group, content } = self else {
            return Ok(())
        };

        if *kind != model.kind || *content != model.content {
            self.delete(model)?;
            self.create_if_needed(model)?;

            return Ok(());
        }

        if *mode != model.mode {
            let perms = Permissions::from_mode(model.mode.bits());

            fs::set_permissions(&model.path, perms).context("Can't set permissions")?;
            *mode = model.mode;
        }

        if *owner != model.owner {
            unistd::chown(&model.path, Some(model.owner), None).context("Can't set owner")?;
            *owner = model.owner;
        }

        if *group != model.group {
            unistd::chown(&model.path, None, Some(model.group)).context("Can't set group")?;
            *group = model.group;
        }

        Ok(())
    }

    /// Deletes this entry on the file system.
    fn delete(&mut self, model: &VirtualFsEntry) -> anyhow::Result<()> {
        match self {
            FsEntry::NonExisting => (),

            FsEntry::Existing {
                kind: EntryKind::File | EntryKind::Symlink,
                ..
            } => fs::remove_file(&model.path).context("Could not delete file")?,

            FsEntry::Existing {
                kind: EntryKind::Directory,
                ..
            } => fs::remove_dir_all(&model.path).context("Could not delete directory")?,
        }

        *self = FsEntry::NonExisting;

        Ok(())
    }
}

impl FsEntry {
    /// Creates a new [`FsEntry`] and retrieves its state from the file system.
    fn create(path: impl AsRef<Path>) -> anyhow::Result<Self> {
        let path = path.as_ref();
        let stat = match stat::stat(path) {
            Ok(s) => s,
            Err(Errno::ENOENT) => return Ok(Self::NonExisting),
            Err(e) => bail!("Cannot stat '{}': {e}", path.to_string_lossy()),
        };

        let flags = SFlag::from_bits_truncate(stat.st_mode);
        let kind = if flags.contains(SFlag::S_IFDIR) {
            EntryKind::Directory
        } else if flags.contains(SFlag::S_IFLNK) {
            EntryKind::Symlink
        } else {
            EntryKind::File
        };

        Ok(Self::Existing {
            kind,
            owner: Uid::from_raw(stat.st_uid),
            group: Gid::from_raw(stat.st_gid),
            mode: Mode::from_bits_truncate(stat.st_mode),
            content: match kind {
                EntryKind::Directory => None,
                EntryKind::File => Some(fs::read(path).context("Can't read file")?),
                EntryKind::Symlink => Some(
                    fs::read_link(path)
                        .context("Can't read link")?
                        .as_os_str()
                        .as_bytes()
                        .to_vec(),
                ),
            },
        })
    }
}

/// Creates, updates or deletes the given entries depending on their configuration.
pub fn process_entries(entries: &[EntryConfiguration]) -> anyhow::Result<()> {
    let mut errors = Vec::with_capacity(entries.len());

    for entry in entries {
        if let Err(e) = process_entry(entry) {
            errors.push(e.context(format!(
                "Could not process entry with path '{}'",
                entry.path.to_string_lossy()
            )));
        }
    }

    if errors.is_empty() {
        Ok(())
    } else {
        let errors = errors
            .into_iter()
            .map(|e| format!(" - {e:?}"))
            .collect::<Vec<_>>()
            .join("\n");

        Err(anyhow!(
            "Errors were encountered while processing some entries:\n{errors}"
        ))
    }
}

/// Processes the given configured entry.
fn process_entry(config: &EntryConfiguration) -> anyhow::Result<()> {
    let vfs_entry = VirtualFsEntry::create(config).context("Could not resolve the configuration")?;
    let mut fs_entry =
        FsEntry::create(&vfs_entry.path).context("Could not resolve the corresponding file system entry")?;

    if vfs_entry.path == Path::new("/")
        || vfs_entry.path.starts_with("/home") && vfs_entry.path.components().count() <= 3
    {
        bail!("Path '{}' seems too risky, aborting", vfs_entry.path.to_string_lossy());
    }

    fs_entry
        .create_if_needed(&vfs_entry)
        .context("Could not create the entry")?;

    if config.should_recreate {
        fs_entry
            .update_if_needed(&vfs_entry)
            .context("Could not update the entry")?;
    }

    Ok(())
}

/// Retrieves the default permission for the given file entry.
fn get_default_permission(entry_kind: EntryKind) -> Mode {
    let mask = stat::umask(Mode::from_bits_truncate(0o22));
    stat::umask(mask);

    match entry_kind {
        EntryKind::Directory => Mode::from_bits_truncate(0o777) - mask,
        _ => Mode::from_bits_truncate(0o666) - mask,
    }
}

/// Creates a directory at the given path along all the intermediate ones and applies the supplied permissions.
fn create_dir_all(dir: impl AsRef<Path>, mode: Mode, owner: Uid, group: Gid) -> anyhow::Result<()> {
    let path = dir.as_ref();
    let mut component_path = PathBuf::with_capacity(path.len());

    if path.is_relative() {
        panic!("The path must be absolute");
    }

    for component in path.components() {
        component_path.push(component);

        if !component_path.is_dir() {
            fs::create_dir(&component_path)
                .with_context(|| format!("Can't create directory '{}'", component_path.to_string_lossy()))?;

            fs::set_permissions(&component_path, Permissions::from_mode(mode.bits()))
                .with_context(|| format!("Can't set permissions for '{}'", component_path.to_string_lossy()))?;

            unistd::chown(&component_path, Some(owner), Some(group)).context("Can't set owner & group")?;
        }
    }

    Ok(())
}

/// Retrieves the content of the given entry configuration.
fn compute_entry_content(config: &EntryConfiguration) -> anyhow::Result<Vec<u8>> {
    let mut bytes = match &config.content {
        EntryContent::String(str) => str.as_bytes().to_vec(),
        EntryContent::Path(path) => {
            fs::read(path).with_context(|| format!("Could not read file '{}'", path.to_string_lossy()))?
        }
    };

    if let Some(path) = &config.transformer {
        bytes = duct::cmd!("nu", "--no-config-file", "--stdin", path)
            .stdin_bytes(bytes)
            .stdout_capture()
            .run()
            .with_context(|| format!("Could not run transformer '{}'", path.to_string_lossy()))?
            .stdout;
    }

    Ok(bytes)
}
