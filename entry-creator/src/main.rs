use crate::cli::Arguments;
use clap::Parser;

mod cli;
mod config;
mod entry_processor;

fn main() -> anyhow::Result<()> {
    let args = Arguments::parse();
    let config = config::parse_file(args.config_file)?;

    entry_processor::process_entries(&config)?;

    Ok(())
}
