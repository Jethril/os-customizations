use clap::Parser;
use std::path::PathBuf;

/// The command-line arguments.
#[derive(Debug, Eq, PartialEq, Clone, Parser)]
#[command(author, version, about)]
pub struct Arguments {
    /// A configuration file that contains the different entries that should be created.
    pub config_file: PathBuf,
}
