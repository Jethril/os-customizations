use anyhow::Context;
use serde::de::Error;
use serde::{Deserialize, Deserializer};
use std::fs::File;
use std::path::{Path, PathBuf};

/// The content of a configured file entry.
#[derive(Debug, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "kebab-case", tag = "type", content = "value")]
pub enum EntryContent {
    /// The content is directly provided as a string.
    String(String),

    /// The content is a path to a file containing the entry's content.
    Path(PathBuf),
}

/// Describes a type of entry.
#[derive(Debug, Deserialize, Eq, PartialEq, Copy, Clone)]
#[serde(rename_all = "kebab-case")]
pub enum EntryKind {
    /// The entry should be a symbolic link.
    Symlink,

    /// The entry should be a file.
    File,

    /// The entry should be a directory.
    Directory,
}

/// The configuration required to create an entry with this tool.
#[derive(Debug, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EntryConfiguration {
    /// The entry type.
    #[serde(rename = "type")]
    pub kind: EntryKind,

    /// The entry's creation path.
    pub path: PathBuf,

    /// The permissions to apply to the entry itself.
    #[serde(deserialize_with = "parse_octal_str")]
    pub mode: Option<u32>,

    /// The name of the owner to use (defaults to the current user).
    pub owner: Option<String>,

    /// The name of the group unix group to use (defaults to the current group).
    pub group: Option<String>,

    /// The permissions to apply to the created directories, or [`None`] to fail if the directory
    /// that should contain the entry does not exist.
    #[serde(deserialize_with = "parse_octal_str")]
    pub directories_modes: Option<u32>,

    /// The entry's content.
    pub content: EntryContent,

    /// A path to a Nushell script that takes the file as input and returns the path.
    pub transformer: Option<PathBuf>,

    /// Whether the entry should be overwritten if it exists and is different or left as-is.
    pub should_recreate: bool,
}

/// Parses configuration file at the given path.
pub fn parse_file(file: impl AsRef<Path>) -> anyhow::Result<Vec<EntryConfiguration>> {
    let file = File::open(file).context("Could not open the configuration file")?;
    let config = serde_json::from_reader(file).context("Could not parse the configuration")?;

    Ok(config)
}

/// Parses an octal number representation as a string to a `u32`.
fn parse_octal_str<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Option<u32>, D::Error> {
    let option: Option<String> = Deserialize::deserialize(deserializer)?;

    match option {
        Some(str) => u32::from_str_radix(&str, 8).map(Some).map_err(Error::custom),
        None => Ok(None),
    }
}
