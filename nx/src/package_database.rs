use crate::fancy_output::FancyOutput;
use crate::nix;
use crate::nix::NIX_CONFIG_DIR;
use anyhow::{anyhow, Context};
use const_str::concat as const_concat;
use rayon::iter::{ParallelBridge, ParallelIterator};
use serde::de::DeserializeOwned;
use serde::Deserialize;
use std::collections::HashMap;
use std::ffi::OsString;
use std::fs;
use std::io::Cursor;
use std::path::{Path, PathBuf};
use tantivy::collector::TopDocs;
use tantivy::query::{Query, QueryParser, TermQuery};
use tantivy::schema::{IndexRecordOption, Schema, STORED, TEXT};
use tantivy::tokenizer::TokenizerManager;
use tantivy::{doc, Document, Index, Term};

const PACKAGE_DATABASE_PATH: &str = "/var/lib/nx";
const INPUTS_INDICES_DIR: &str = const_concat!(PACKAGE_DATABASE_PATH, "/system_inputs_indices");
const REG_INDICES_DIR: &str = const_concat!(PACKAGE_DATABASE_PATH, "/registries_indices");

/// Indicates that the search could not be performed.
pub enum SearchError {
    /// The provided query is invalid.
    InvalidQuerySyntax,

    /// Something bad happened.
    Other(anyhow::Error),
}

impl From<anyhow::Error> for SearchError {
    fn from(value: anyhow::Error) -> Self {
        Self::Other(value)
    }
}

/// Contains information about the available Nix packages.
pub struct PackageDatabase {
    /// The current system.
    system: String,

    /// The NixOS version that runs this program.
    nixos_version: String,

    /// The index schema.
    schema: Schema,

    /// The indices for each system input.
    system_inputs_indices: HashMap<String, Index>,

    /// The indices for each registry.
    registries_indices: HashMap<String, Index>,
}

impl PackageDatabase {
    /// Loads the package database.
    pub fn load() -> anyhow::Result<Self> {
        let system = nix::get_current_system().context("Could not get the system")?;
        let nixos_version = nix::get_nixos_version().context("Could not get the NixOS version")?;

        Ok(Self {
            system,
            nixos_version,
            schema: PackageInfo::schema(),
            system_inputs_indices: read_indices(INPUTS_INDICES_DIR),
            registries_indices: read_indices(REG_INDICES_DIR),
        })
    }

    /// Returns an iterator over the different system inputs.
    pub fn system_inputs(&self) -> impl Iterator<Item = &String> {
        self.system_inputs_indices.keys()
    }

    /// Retrieves all registries that contain the given package, or who contain a name that looks like the given one.
    ///
    /// # Returns
    /// A hashmap that associates the registries names (keys) with the package name (values). When an exact match has
    /// been found, a string that equals the `name` parameter is returned as value. Else, the closest package name is
    /// returned.
    pub fn get_registries_for_package(&self, name: &str) -> anyhow::Result<HashMap<String, String>> {
        let mut result = HashMap::with_capacity(self.registries_indices.len());
        let name_field = self.schema.get_field(PackageInfo::NAME_FIELD).unwrap();
        let query = TermQuery::new(Term::from_field_text(name_field, name), IndexRecordOption::Basic);
        let collector = TopDocs::with_limit(1);

        for (registry_name, index) in &self.registries_indices {
            let searcher = index.reader().context("Could not create the reader")?.searcher();
            let res = searcher
                .search(&query, &collector)
                .context("Unable to search an index")?
                .into_iter()
                .filter_map(|(_, addr)| searcher.doc(addr).ok())
                .filter_map(|doc| PackageInfo::try_from(doc).ok())
                .next();

            if let Some(package) = res {
                result.insert(registry_name.clone(), package.name);
            }
        }

        Ok(result)
    }

    /// Searches packages that contain the given query string.
    pub fn search_package_by_query(&self, query: &str, max_results: usize) -> Result<SearchResult, SearchError> {
        let query = self.parse_query(query)?;
        let collector = TopDocs::with_limit(max_results);
        let mut result = SearchResult::default();

        for (input_name, index) in &self.system_inputs_indices {
            let searcher = index.reader().context("Unable to read an index")?.searcher();

            let results = searcher
                .search(&query, &collector)
                .context("Unable to search an index")?
                .into_iter()
                .filter_map(|(_, addr)| searcher.doc(addr).ok())
                .filter_map(|doc| PackageInfo::try_from(doc).ok())
                .collect::<Vec<_>>();

            if !results.is_empty() {
                result.system_inputs.insert(input_name.clone(), results);
            }
        }

        for (registry_name, index) in &self.registries_indices {
            let searcher = index.reader().context("Unable to read an index")?.searcher();

            let results = searcher
                .search(&query, &collector)
                .context("Unable to search an index")?
                .into_iter()
                .filter_map(|(_, addr)| searcher.doc(addr).ok())
                .filter_map(|doc| PackageInfo::try_from(doc).ok())
                .collect::<Vec<_>>();

            if !results.is_empty() {
                result.registries.insert(registry_name.clone(), results);
            }
        }

        Ok(result)
    }

    /// Updates the package database, outputting to `output`.
    pub fn update(&mut self, output: &mut FancyOutput) -> anyhow::Result<()> {
        output.set_message(Some("Enumerating system flake inputs"));
        let system_inputs = nix::get_system_flake_inputs()
            .context("Could not get the system inputs")?
            .into_iter()
            .map(|name| (Some(NIX_CONFIG_DIR), Path::new(INPUTS_INDICES_DIR).join(&name), name));

        output.set_message(Some("Enumerating system registries"));
        let registries = nix::get_system_registries()
            .context("Could not get the registries")?
            .into_iter()
            .map(|name| (None, Path::new(REG_INDICES_DIR).join(&name), name));

        output.set_message(Some("Enumerating packages"));
        let items = system_inputs
            .chain(registries)
            .par_bridge()
            .map(|(source, dir, name)| Ok((self.index_flake(&name, dir, source)?, name, source.is_some())))
            .collect::<anyhow::Result<Vec<_>>>()
            .context("Could not retrieve package metadata")?;

        self.registries_indices.clear();
        self.system_inputs_indices.clear();

        output.set_message(Some("Updating indices"));
        for (packages, name, is_system_package) in items {
            if is_system_package {
                self.system_inputs_indices.insert(name, packages);
            } else {
                self.registries_indices.insert(name, packages);
            }
        }

        output.set_message(None);
        Ok(())
    }

    /// Parses the given query string to a query.
    fn parse_query(&self, query: &str) -> Result<Box<dyn Query>, SearchError> {
        let name_field = self.schema.get_field(PackageInfo::NAME_FIELD).unwrap();
        let desc_field = self.schema.get_field(PackageInfo::DESCRIPTION_FIELD).unwrap();
        let tokenizer = TokenizerManager::default();

        QueryParser::new(self.schema.clone(), vec![name_field, desc_field], tokenizer)
            .parse_query(query)
            .map_err(|_| SearchError::InvalidQuerySyntax)
    }

    /// Retrieves all packages for the given flake and creates an index from them.
    ///
    /// # Parameters
    /// - `flake`: the flake.
    /// - `index_dir`: the index directory.
    /// - `inputs_source`: the registries to search (providing [None] results in the system inputs being searched).
    fn index_flake(&self, flake: &str, index_dir: PathBuf, inputs_source: Option<&str>) -> anyhow::Result<Index> {
        const BATCH_SIZE: usize = 1000;

        let schema = PackageInfo::schema();
        let package_names = self
            .query_flake::<Vec<String>>("builtins.attrNames", flake, inputs_source)
            .context("Could not query the package list")?;

        let _ = fs::remove_dir_all(&index_dir);
        fs::create_dir_all(&index_dir)
            .with_context(|| format!("Could not create the index directory '{index_dir:?}'"))?;

        let result = Index::create_in_dir(index_dir, schema).context("Could not create the index")?;
        let mut writer = result.writer(50_000_000).context("Could not create the index writer")?;

        for package_names_chunk in package_names.chunks(BATCH_SIZE) {
            let attributes = package_names_chunk
                .iter()
                .map(serde_json::to_string)
                .collect::<serde_json::Result<Vec<_>>>()
                .unwrap()
                .join(" ");

            let query = format!(
                r#"
            let
              attrs = [ {attributes} ];
              ifNotNull = v: action: if v == null then null else action;
              try = attrs: key:
                let
                  res = builtins.tryEval attrs.${{key}} or null;
                in
                  if res.success then
                    res.value
                  else
                    null;
               in
                 pkgs: builtins.map (name:
                   let
                     pkg = pkgs.${{name}};
                     rawVersion = try pkg "version";
                     meta = try pkg "meta";
                     homepage = try meta "homepage";
                   in
                     {{
                       inherit name;

                       version =
                         if builtins.isString rawVersion then
                           rawVersion
                         else if builtins.isInt rawVersion then
                           toString rawVersion
                         else
                           null;

                       homepage = ifNotNull meta (
                         if builtins.isList homepage then
                           builtins.head homepage
                         else
                           homepage
                       );

                       description = ifNotNull meta (try meta "description");
                       source = ifNotNull meta (try meta "position");
                       mainProgram = ifNotNull meta (try meta "mainProgram");
                     }}
                 ) attrs
        "#
            );

            let package_infos = self
                .query_flake::<Vec<PackageInfo>>(&query, flake, inputs_source)
                .context("Could not retrieve package infos")?;

            for mut pkg in package_infos {
                let pkgs_index = pkg.source.as_ref().and_then(|src| src.find("/pkgs/"));

                if let Some(pkgs_index) = pkgs_index {
                    let src = pkg.source.as_ref().unwrap();

                    if let Some(line_pos) = src.rfind(':') {
                        let relative_url = &src[pkgs_index..line_pos];
                        let line = &src[line_pos + 1..];

                        pkg.source = Some(format!(
                            "https://github.com/NixOS/nixpkgs/blob/nixos-{}{relative_url}#L{line}",
                            self.nixos_version
                        ));
                    }
                }

                writer.add_document(pkg.into()).context("Unable to write document")?;
            }
        }

        writer.commit().context("Could not commit index files")?;

        Ok(result)
    }

    /// Runs a Nix query (expression) against the package manager, with the scope of the supplied flake and deserializes
    /// its output in an object.
    ///
    /// # Parameters
    /// - `expr` An expression that returns a lambda that will be invoked with each package of the given flake.
    /// - `flake` The flake.
    /// - `inputs_source` The registries to search (providing [None] results in the system inputs being searched).
    fn query_flake<T: DeserializeOwned>(
        &self,
        expr: &str,
        flake: &str,
        inputs_source: Option<&str>,
    ) -> anyhow::Result<T> {
        let mut args: Vec<OsString> = vec![
            "eval".into(),
            "--impure".into(),
            "--no-write-lock-file".into(),
            "--show-trace".into(),
            "--json".into(),
            format!("{flake}#legacyPackages.{}", self.system).into(),
            "--apply".into(),
            expr.into(),
        ];

        if let Some(inputs_source) = inputs_source {
            args.push("--inputs-from".into());
            args.push(inputs_source.into());
        }

        let output = duct::cmd("nix", args)
            .env("NIXPKGS_ALLOW_INSECURE", "1")
            .read()
            .context("Could not run the nix command")?;

        serde_json::from_reader(Cursor::new(output)).context("Could not parse the JSON output")
    }
}

/// A search result.
#[derive(Debug, Default, Clone)]
pub struct SearchResult {
    /// The matching packages for each system input.
    pub system_inputs: HashMap<String, Vec<PackageInfo>>,

    /// The matching packages for each registry.
    pub registries: HashMap<String, Vec<PackageInfo>>,
}

/// Contain information about a package.
#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PackageInfo {
    /// The package's name.
    pub name: String,

    /// The package's version.
    pub version: Option<String>,

    /// A short description.
    pub description: Option<String>,

    /// A URL that points to the package's home page.
    pub homepage: Option<String>,

    /// The name of the main program contained in the package.
    pub main_program: Option<String>,

    /// A URL that points to the package's definition in Nixpkgs.
    pub source: Option<String>,
}

impl PackageInfo {
    const NAME_FIELD: &'static str = "name";
    const VERSION_FIELD: &'static str = "version";
    const DESCRIPTION_FIELD: &'static str = "description";
    const HOMEPAGE_FIELD: &'static str = "homepage";
    const MAIN_PROGRAM_FIELD: &'static str = "main_program";
    const SOURCE_FIELD: &'static str = "source";

    /// Creates a schema from the package info fields.
    fn schema() -> Schema {
        let mut builder = Schema::builder();
        builder.add_text_field(Self::NAME_FIELD, TEXT | STORED);
        builder.add_text_field(Self::VERSION_FIELD, STORED);
        builder.add_text_field(Self::DESCRIPTION_FIELD, TEXT | STORED);
        builder.add_text_field(Self::HOMEPAGE_FIELD, STORED);
        builder.add_text_field(Self::MAIN_PROGRAM_FIELD, STORED);
        builder.add_text_field(Self::SOURCE_FIELD, STORED);
        builder.build()
    }
}

impl TryFrom<Document> for PackageInfo {
    type Error = anyhow::Error;

    fn try_from(value: Document) -> Result<Self, Self::Error> {
        let schema = Self::schema();

        Ok(Self {
            name: value
                .get_first(schema.get_field(Self::NAME_FIELD).unwrap())
                .ok_or_else(|| anyhow!("The document should have a name"))?
                .as_text()
                .expect("should be a string")
                .to_owned(),

            version: value
                .get_first(schema.get_field(Self::VERSION_FIELD).unwrap())
                .map(|v| v.as_text().expect("should be a string").to_owned()),

            description: value
                .get_first(schema.get_field(Self::DESCRIPTION_FIELD).unwrap())
                .map(|v| v.as_text().expect("should be a string").to_owned()),

            homepage: value
                .get_first(schema.get_field(Self::HOMEPAGE_FIELD).unwrap())
                .map(|v| v.as_text().expect("should be a string").to_owned()),

            main_program: value
                .get_first(schema.get_field(Self::MAIN_PROGRAM_FIELD).unwrap())
                .map(|v| v.as_text().expect("should be a string").to_owned()),

            source: value
                .get_first(schema.get_field(Self::SOURCE_FIELD).unwrap())
                .map(|v| v.as_text().expect("should be a string").to_owned()),
        })
    }
}

impl From<PackageInfo> for Document {
    fn from(value: PackageInfo) -> Self {
        let schema = PackageInfo::schema();
        let name = schema.get_field(PackageInfo::NAME_FIELD).unwrap();
        let mut result = Document::new();

        result.add_text(name, value.name);

        if let Some(version) = value.version {
            result.add_text(schema.get_field(PackageInfo::VERSION_FIELD).unwrap(), version);
        }

        if let Some(description) = value.description {
            result.add_text(schema.get_field(PackageInfo::DESCRIPTION_FIELD).unwrap(), description);
        }

        if let Some(homepage) = value.homepage {
            result.add_text(schema.get_field(PackageInfo::HOMEPAGE_FIELD).unwrap(), homepage);
        }

        if let Some(main_program) = value.main_program {
            result.add_text(schema.get_field(PackageInfo::MAIN_PROGRAM_FIELD).unwrap(), main_program);
        }

        if let Some(source) = value.source {
            result.add_text(schema.get_field(PackageInfo::SOURCE_FIELD).unwrap(), source);
        }

        result
    }
}

/// Reads the available indices in a directory.
fn read_indices(directory: &str) -> HashMap<String, Index> {
    let mut result = HashMap::new();

    if let Ok(dir_entries) = fs::read_dir(directory) {
        for entry in dir_entries {
            let Ok(entry) = entry else { continue; };
            let Ok(name) = entry.file_name().into_string() else { continue; };
            let Ok(index) = Index::open_in_dir(entry.path()) else { continue; };

            result.insert(name, index);
        }
    }

    result
}
