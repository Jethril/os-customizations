//! Miscellaneous utilities.

use crate::fancy_output::FancyOutput;
use anyhow::{anyhow, Context};
use duct::Expression;
use std::io::Read;

/// Trait that allows extensions for commands.
pub trait CommandFancyOutput
where
    Self: Sized,
{
    /// Returns an error if the command failed to run, outputting lines to the fancy output message.
    fn run_with_output(&mut self, output: &mut FancyOutput) -> anyhow::Result<String>;
}

impl CommandFancyOutput for Expression {
    fn run_with_output(&mut self, output: &mut FancyOutput) -> anyhow::Result<String> {
        let reader = self
            .stderr_to_stdout()
            .reader()
            .context("Could not find the executable")?;

        let mut output_buffer = String::with_capacity(1024 * 10);
        let mut last_line_return = 0;

        for byte in reader.bytes() {
            let byte = match byte {
                Ok(b) => b,
                Err(e) => {
                    output.prepare_for_println();
                    println!("{}", output_buffer);

                    return Err(anyhow!(e));
                }
            };

            if byte == b'\n' {
                output.set_message(Some(&output_buffer[last_line_return + 1..]));
                last_line_return = output_buffer.len();
            }

            output_buffer.push(byte as char);
        }

        Ok(output_buffer)
    }
}
