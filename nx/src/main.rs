use crate::config_manager::ActivationMode;
use crate::fancy_output::FancyOutput;
use crate::package_database::{PackageDatabase, PackageInfo, SearchError};
use anyhow::Context;
use clap::Parser;
use cli::{Arguments, Command::*};
use colored::Colorize;
use duct::cmd;
use std::collections::{HashMap, HashSet};
use std::num::NonZeroU8;
use std::os::fd::AsRawFd;
use std::{io, process};

mod cli;
mod config_manager;
mod fancy_output;
mod misc;
mod nix;
mod package_database;

fn main() {
    let args = Arguments::parse();
    let mut output = FancyOutput::new(!args.no_fancy);

    let result = match args.command {
        UpdatePackageDatabase { in_place } if in_place => update_package_database_in_place(&mut output),
        UpdatePackageDatabase { .. } => update_package_database_in_background(&mut output),
        Update => update(&mut output),
        Rollback => rebuild_configuration(&mut output, ActivationMode::Switch, false, true),
        Switch { update } => rebuild_configuration(&mut output, ActivationMode::Switch, update, false),
        Test { update } => rebuild_configuration(&mut output, ActivationMode::Test, update, false),
        Boot { update } => rebuild_configuration(&mut output, ActivationMode::Boot, update, false),
        DryBuild => rebuild_configuration(&mut output, ActivationMode::Dry, false, false),
        Shell { packages } => match shell(packages) {
            Ok(()) => return,
            res => res,
        },

        Search {
            terms,
            max_results,
            include_registries,
        } => search(&mut output, terms, max_results, include_registries),

        CollectGarbage { generations } => collect_garbage(&mut output, generations),
    };

    if let Err(err) = result {
        output.log_error(err);
        process::exit(5);
    }

    output.done();
}

/// Updates the package database.
fn update_package_database_in_place(output: &mut FancyOutput) -> anyhow::Result<()> {
    output.set_task("Updating package database");
    output.set_message(Some("Loading package database"));
    let mut db = PackageDatabase::load()?;

    output.set_message(None);
    db.update(output).context("Unable to update the database")?;

    Ok(())
}

/// Invokes the [`update_package_database_in_place`] inside a service, in background.
fn update_package_database_in_background(output: &mut FancyOutput) -> anyhow::Result<()> {
    cmd!("systemctl", "start", "nx-update-package-database.service")
        .run()
        .context("Could not run the systemctl executable")?;

    output.prepare_for_println();
    println!("The package database is being updated in background.");
    println!(
        "{} {} {}",
        "Call with the".bright_black(),
        "--in-place".bold(),
        "flag to wait for it to end.".bright_black(),
    );

    Ok(())
}

/// Invalidates the system flake and registries lock files.
fn update(output: &mut FancyOutput) -> anyhow::Result<()> {
    output.set_task("Updating the configuration");
    config_manager::update_config(output).context("Could not update the configuration")?;

    Ok(())
}

/// Starts a nix shell with the given packages.
fn shell(packages: Vec<String>) -> anyhow::Result<()> {
    let package_database = PackageDatabase::load().context("Could not load the package database")?;
    let flake_inputs = package_database.system_inputs().collect::<HashSet<_>>();
    let mut packages_with_registries = HashSet::with_capacity(packages.len());

    for package in packages {
        if package.contains('#') {
            packages_with_registries.insert(package);
            continue;
        }

        let mut matches = package_database
            .get_registries_for_package(&package)
            .context("Could not read the packages")?
            .into_iter()
            .collect::<Vec<_>>();

        matches.sort_by_key(|(registry_name, _)| !flake_inputs.contains(registry_name));

        if let Some((registry_name, package_name)) = matches.iter().find(|(_, name)| *name == package) {
            packages_with_registries.insert(format!("{registry_name}#{package_name}"));
            continue;
        }

        let possible_names = matches
            .into_iter()
            .map(|(_, name)| name)
            .collect::<HashSet<_>>()
            .into_iter()
            .collect::<Vec<_>>()
            .join(", ");

        println!("Unable to find a package with name {}.", package.bold());

        if !possible_names.is_empty() {
            println!("  Did you mean: {possible_names}");
        }
    }

    {
        let using_str = packages_with_registries
            .iter()
            .map(|v| v.cyan().to_string())
            .collect::<Vec<_>>()
            .join(", ");

        println!("Using {using_str}.");
    }

    let mut args = Vec::with_capacity(packages_with_registries.len() + 3);
    args.push("shell");
    args.extend(packages_with_registries.iter().map(|v| v.as_str()));
    args.push("--no-write-lock-file");
    args.push("--impure");

    duct::cmd("nix", args)
        .stdout_file(io::stdout().as_raw_fd())
        .stderr_file(io::stderr().as_raw_fd())
        .run()?;

    println!("Shell exited.");

    Ok(())
}

/// Searches the package database.
fn search(output: &mut FancyOutput, terms: Vec<String>, max_results: usize, registries: bool) -> anyhow::Result<()> {
    fn print_package_info(info: &PackageInfo) {
        print!("- {}", info.name.green());

        if let Some(version) = &info.version {
            print!(" {}", format!("({version})").blue());
        }

        if let Some(desc) = &info.description {
            print!(": {}", desc.italic().bright_black());
        }

        println!();

        if let Some(homepage_url) = &info.homepage {
            println!("    Homepage: {homepage_url}");
        }

        if let Some(source) = &info.source {
            println!("    Defined in: {source}");
        }

        if let Some(executable) = &info.main_program {
            println!("    Main program: {executable}");
        }
    }

    output.set_task("Loading package database");
    let database = PackageDatabase::load()?;

    output.set_task("Searching");
    let mut result = match database.search_package_by_query(&terms.join(" "), max_results) {
        Ok(res) => res,
        Err(SearchError::Other(error)) => return Err(error),
        Err(SearchError::InvalidQuerySyntax) => {
            eprintln!("Can't parse the search terms");
            return Ok(());
        }
    };

    if !registries || result.system_inputs.is_empty() {
        result.registries = HashMap::default();
    }

    let separator = "==============".blue();
    let total_results: usize = result
        .system_inputs
        .values()
        .chain(result.registries.values())
        .map(|v| v.len())
        .sum();

    output.prepare_for_println();

    if total_results == 0 {
        println!("No corresponding packages were found.");
        return Ok(());
    }

    println!("Found {} corresponding packages.", total_results.to_string().bold());

    for (input_name, packages) in result.system_inputs {
        println!("{separator} on input '{input_name}' {separator}");

        for package in packages {
            print_package_info(&package);
        }
    }

    for (registry_name, packages) in result.registries {
        println!("{separator} on registry '{registry_name}' {separator}");

        for package in packages {
            print_package_info(&package);
        }
    }

    println!();

    Ok(())
}

/// Collects unused derivations.
fn collect_garbage(output: &mut FancyOutput, generations: NonZeroU8) -> anyhow::Result<()> {
    output.set_task("Collecting old generations and optimizing store...");

    let reclaimed = nix::collect_garbage(output, generations).context("Could not collect garbage")?;

    output.prepare_for_println();
    println!("Reclaimed {reclaimed:.2} GB of space.");

    Ok(())
}

/// Rebuilds the system configuration.
fn rebuild_configuration(
    output: &mut FancyOutput,
    mode: ActivationMode,
    update: bool,
    rollback: bool,
) -> anyhow::Result<()> {
    if update {
        output.set_task("Updating the configuration");
        config_manager::update_config(output).context("Could not update the configuration")?;
    }

    output.set_task("Switching to the new configuration");
    let result = config_manager::rebuild_config(mode, rollback, output).context("Cannot rebuild switch")?;

    output.prepare_for_println();

    for warning_message in result.warning_messages {
        println!("{}: {warning_message}", "warning".yellow());
    }

    for trace_message in result.trace_messages {
        println!("{}: {trace_message}", "trace".bright_blue());
    }

    update_package_database_in_background(output)
}
