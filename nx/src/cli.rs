use clap::{Parser, Subcommand, ValueEnum};
use std::num::NonZeroU8;

/// The root command-line arguments.
#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct Arguments {
    /// The command to run.
    #[command(subcommand)]
    pub command: Command,

    /// Disables the "fancy display mode" and shows the command's raw output.
    #[arg(long)]
    pub no_fancy: bool,
}

/// The different available sub-commands.
#[derive(Debug, Clone, Subcommand)]
pub enum Command {
    /// Updates the package index.
    UpdatePackageDatabase {
        /// Runs the update in-place rather than running it inside the system service.
        #[arg(long)]
        in_place: bool,
    },

    /// Updates the system flake dependencies by revoking `flake.lock`.
    #[command(alias("u"))]
    Update,

    /// Cancels the last applied configuration (tested or switched).
    Rollback,

    /// Builds the updated configuration and switches to it.
    #[command(alias("sw"))]
    Switch {
        /// Performs an update before switching.
        #[arg(long)]
        update: bool,
    },

    /// Builds the updated configuration and switches to it until the next boot (or rollback).
    Test {
        /// Performs an update before switching.
        #[arg(long)]
        update: bool,
    },

    /// Builds the updated configuration and waits for the next boot to switch to it.
    Boot {
        /// Performs an update before switching.
        #[arg(long)]
        update: bool,
    },

    /// Dry-builds the configuration.
    DryBuild,

    /// Runs a Nix shell with the given packages.
    #[command(alias("sh"))]
    Shell {
        /// The packages the shell should provide.
        packages: Vec<String>,
    },

    /// Searches the supplied package or command.
    #[command(alias("s"))]
    Search {
        /// The search keywords.
        terms: Vec<String>,

        /// The maximum number of results to display.
        #[arg(long, default_value_t = 5)]
        max_results: usize,

        /// Whether to include registries in the search.
        #[arg(long, short = 'r')]
        include_registries: bool,
    },

    /// Collects the old generations and optimizes the store.
    #[command(alias("gc"))]
    CollectGarbage {
        /// The number of generations to keep.
        #[arg(long, default_value_t = NonZeroU8::new(3).unwrap())]
        generations: NonZeroU8,
    },
}

/// The type of project that should be created.
#[derive(Debug, Eq, PartialEq, Copy, Clone, ValueEnum)]
pub enum ProjectKind {
    /// Creates a Rust project.
    Rust,

    /// Creates a JVM project.
    Jvm,

    /// Creates a .NET project.
    Dotnet,

    /// Creates a blank project.
    Blank,
}
