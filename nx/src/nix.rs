use crate::fancy_output::FancyOutput;
use crate::misc::CommandFancyOutput;
use anyhow::{bail, Context};
use const_str::concat as const_concat;
use duct::cmd;
use lazy_regex::{regex, regex_captures};
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::num::NonZeroU8;
use std::{fs, io};

pub const NIX_CONFIG_DIR: &str = "/etc/nixos";
pub const SYSTEM_PROFILE_DIR: &str = const_concat!(PROFILES_DIR, "/system");
const PROFILES_DIR: &str = "/nix/var/nix/profiles";
const NIX_FLAKE: &str = const_concat!(NIX_CONFIG_DIR, "/flake.nix");

/// Retrieves the current NixOS system.
pub fn get_current_system() -> io::Result<String> {
    const SYSTEM_FILE_PATH: &str = const_concat!(SYSTEM_PROFILE_DIR, "/system");

    fs::read_to_string(SYSTEM_FILE_PATH).map(|sys| sys.trim().to_owned())
}

/// Retrieves the current NixOS version.
pub fn get_nixos_version() -> io::Result<String> {
    const NIXOS_VERSION_PATH: &str = const_concat!(SYSTEM_PROFILE_DIR, "/nixos-version");

    fs::read_to_string(NIXOS_VERSION_PATH).map(|sys| sys.trim()[0..5].to_owned())
}

/// Retrieves the system flake inputs.
pub fn get_system_flake_inputs() -> anyhow::Result<HashSet<String>> {
    let output = cmd!("nix", "eval", "inputs", "--file", NIX_FLAKE, "--json")
        .read()
        .context("Could not find the nix executable")?;

    #[derive(Deserialize)]
    struct Empty {}

    Ok(serde_json::from_str::<HashMap<String, Empty>>(&output)
        .context("Could not deserialize the JSON output")?
        .into_keys()
        .collect())
}

/// Retrieves the system registries' names.
pub fn get_system_registries() -> anyhow::Result<HashSet<String>> {
    let line_regex = regex!("^system flake:([^ ]+)");
    let output = cmd!("nix", "registry", "list")
        .read()
        .context("Could not run the nix executable")?;

    Ok(output
        .lines()
        .filter_map(|l| line_regex.captures(l))
        .map(|cap| cap.get(1).unwrap().as_str().to_owned())
        .filter(|v| !v.is_empty())
        .collect())
}

/// Collects old generation from the nix store.
pub fn collect_garbage(output: &mut FancyOutput, generations: NonZeroU8) -> anyhow::Result<f64> {
    cmd!("nix-env", "--delete-generations", format!("+{generations}"))
        .run()
        .context("Could not run the 'nix-env' executable")?;

    let store_output = cmd!("nix", "store", "gc")
        .read()
        .context("Could not run the 'nix' executable")?;

    cmd!("nix", "store", "optimise")
        .run_with_output(output)
        .context("Could not run the 'nix' executable")?;

    let Some((_, mibs)) = regex_captures!(r#"(\d+\.\d+) MiB freed"#, &store_output) else {
        bail!("Could not match 'nix store' output: {store_output}");
    };

    Ok(
        mibs.parse::<f64>().expect("should be a valid float after regex match") // MiB 
            * 1024.0 // KiB
            * 1024.0 // B
            / 1000.0 // KB
            / 1000.0 // MB
            / 1000.0, // GB
    )
}
