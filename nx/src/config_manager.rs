use crate::fancy_output::FancyOutput;
use crate::misc::CommandFancyOutput;
use crate::nix;
use crate::nix::NIX_CONFIG_DIR;
use anyhow::Context;
use duct::cmd;
use std::borrow::Cow;
use std::path::Path;

/// Invalidates the current system configuration lock file.
pub fn update_config(output: &mut FancyOutput) -> anyhow::Result<()> {
    let system_inputs = nix::get_system_flake_inputs()?;
    let mut args = Vec::with_capacity(2 * (system_inputs.len() + 1));

    args.push("flake");
    args.push("lock");
    args.extend(system_inputs.iter().flat_map(|v| ["--update-input", v]));

    duct::cmd("nix", args)
        .dir(NIX_CONFIG_DIR)
        .run_with_output(output)
        .context("Could not the 'nix' executable")?;

    Ok(())
}

/// Rebuilds the system configuration.
///
/// # Parameters
///  - `mode`: how the configuration that has been built should be activated
///  - `rollback`: whether the previous system configuration should be used instead of a new one
///  - `dry`: whether the configuration should be built without
pub fn rebuild_config(
    mode: ActivationMode,
    rollback: bool,
    output: &mut FancyOutput,
) -> anyhow::Result<EvaluationResult> {
    const EVALUATION_WARNING_INFIX: &str = "evaluation warning:";
    const TRACE_PREFIX: &str = "trace:";

    let mut result = EvaluationResult::default();
    let mut nix_env_args = Vec::with_capacity(5);
    let config_path: Cow<str>;
    nix_env_args.push("-p");
    nix_env_args.push(nix::SYSTEM_PROFILE_DIR);

    if mode == ActivationMode::Dry {
        nix_env_args.push("--dry-run");
    }

    if rollback {
        config_path = nix::SYSTEM_PROFILE_DIR.into();
        nix_env_args.push("--rollback");
    } else {
        let hostname = gethostname::gethostname();
        let hostname = hostname.to_string_lossy();

        let command_output = cmd!(
            "nix",
            "eval",
            "--raw",
            format!("{NIX_CONFIG_DIR}#nixosConfigurations.\"{hostname}\".config.system.build.toplevel.drvPath"),
            "--show-trace"
        )
        .run_with_output(output)
        .context("Could not run the 'nix' executable")?;

        for line in command_output.lines() {
            let Some(mut line) = line.strip_prefix(TRACE_PREFIX) else {
                continue;
            };

            line = line.trim();

            if let Some(line) = line.strip_prefix(EVALUATION_WARNING_INFIX) {
                result.warning_messages.push(line.trim_start().to_owned());
            } else {
                result.trace_messages.push(line.to_owned());
            }
        }

        let drv_path = command_output
            .lines()
            .rfind(|l| l.ends_with(".drv"))
            .context("Could not find the drv path that was built")?;

        let command_output = cmd!("nix-store", "-r", drv_path)
            .run_with_output(output)
            .context("Could not run the 'nix-store' executable")?;

        config_path = command_output
            .lines()
            .rfind(|l| l.starts_with("/nix/store"))
            .context("Could not find the realized store path")?
            .to_owned()
            .into();

        nix_env_args.push("--set");
        nix_env_args.push(&config_path);
    }

    duct::cmd("nix-env", nix_env_args)
        .run_with_output(output)
        .context("Could not run the 'nix-env' executable")?;

    if mode == ActivationMode::Dry {
        return Ok(result);
    }

    let switch_to_configuration_path = Path::new(config_path.as_ref()).join("bin/switch-to-configuration");

    cmd!(
        switch_to_configuration_path,
        mode.as_switch_to_configuration_arg().unwrap()
    )
    .run_with_output(output)
    .context("Could not run the 'switch-to-configuration' executable")?;

    Ok(result)
}

/// Describes a result of building a system configuration.
#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct EvaluationResult {
    /// Warning messages, if any.
    pub warning_messages: Vec<String>,

    /// Trace (debug) messages, if any.
    pub trace_messages: Vec<String>,
}

/// Describes how a new configuration should be handled.
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum ActivationMode {
    /// The system should immediately switch to the new configuration.
    Switch,

    /// The system should switch to the configuration on the next boot.
    Boot,

    /// The system should immediately switch to the new configuration, but it should be kept active until the next boot.
    Test,

    /// Nothing should be done.
    Dry,
}

impl ActivationMode {
    /// Maps this activation mode to a "switch-to-configuration' argument.
    const fn as_switch_to_configuration_arg(self) -> Option<&'static str> {
        match self {
            ActivationMode::Switch => Some("switch"),
            ActivationMode::Boot => Some("boot"),
            ActivationMode::Test => Some("test"),
            ActivationMode::Dry => None,
        }
    }
}
