use colored::Colorize;
use std::io;
use std::io::Write;
use std::time::Instant;

/// Terminal escape sequence used to clear a line.
const CLEAR_RIGHT_ESCAPE_SEQ: &str = "\x1B[0K";

/// Convenience structure that allows printing smart logs into the console.
#[derive(Debug)]
pub struct FancyOutput {
    /// When the operation started.
    start: Instant,

    /// The current output state.
    state: State,
}

impl FancyOutput {
    /// Creates a fancy output with the option to enable or disable it.
    #[inline]
    pub fn new(enabled: bool) -> Self {
        let state = if enabled {
            State::Enabled {
                current_task: "Initializing".to_owned(),
                last_message: None,
            }
        } else {
            State::Disabled
        };

        Self {
            start: Instant::now(),
            state,
        }
    }

    /// Prepares the output for regular println.
    pub fn prepare_for_println(&self) {
        println!("\r{CLEAR_RIGHT_ESCAPE_SEQ}");
    }

    /// Sets a message indicating what the program is doing.
    pub fn set_task(&mut self, task: &str) {
        match &mut self.state {
            State::Enabled {
                current_task,
                last_message,
            } => {
                *current_task = task.to_owned();
                pretty_print_state(current_task, last_message);
            }
            State::Disabled => {}
        }
    }

    /// Sets a message after the task indicating the progress of the task.
    pub fn set_message(&mut self, message: Option<&str>) {
        match &mut self.state {
            State::Enabled {
                last_message,
                current_task,
            } => {
                *last_message = message.map(|m| m.to_owned());
                pretty_print_state(current_task, last_message);
            }
            State::Disabled => {
                if let Some(message) = message {
                    println!("\t{message}");
                }
            }
        }
    }

    /// Logs an error in the console.
    pub fn log_error(&self, error: anyhow::Error) {
        eprintln!(
            "\r{CLEAR_RIGHT_ESCAPE_SEQ}{}: could not perform the requested operation: {error:?}",
            "error".red()
        );
    }

    /// Shows a "Done" message with the total time the program took since this instance of fancy output has been
    /// created.
    pub fn done(self) {
        println!(
            "\r{CLEAR_RIGHT_ESCAPE_SEQ}{}. Took {:.2} seconds.{CLEAR_RIGHT_ESCAPE_SEQ}",
            "Done".green(),
            self.start.elapsed().as_secs_f64()
        );
    }
}

/// The output state.
#[derive(Debug)]
enum State {
    /// The fancy output is enabled and tasks and messages are displayed.
    Enabled {
        current_task: String,
        last_message: Option<String>,
    },

    /// The fancy output is disabled.
    Disabled,
}

/// Convenience function that prints a task and a message to the console, erasing the output produced by a previous call
/// of this method.
fn pretty_print_state(task: &str, message: &Option<String>) {
    print!("\r{CLEAR_RIGHT_ESCAPE_SEQ}{}", task.bold());

    if let Some(message) = message.as_ref() {
        print!(" - {}", message.italic());
    }

    let _ = io::stdout().flush();
}
