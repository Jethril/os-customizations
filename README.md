# Jethril's OS customizations

Several small programs and utilities to customize the OS / enhance the daily workflow of my NixOS desktop.

## Project runner

A little Rust program that allows opening project folders with my favorite tools, based on a `flake.nix` file.

## nx

A `nix` command-line wrapper that eases the use of NixOS for my own needs.

## Entry creator

A little Rust program that allows creating / updating file system entries from a configuration file in a service.
